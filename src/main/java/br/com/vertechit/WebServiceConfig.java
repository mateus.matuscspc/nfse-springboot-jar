package br.com.vertechit;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {
	
	@Bean
	public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
		
		MessageDispatcherServlet servlet = new MessageDispatcherServlet();
		servlet.setApplicationContext    (applicationContext);
		servlet.setTransformWsdlLocations(true);
		
		ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(servlet, "/ws/*");
		
		return servletRegistrationBean;
	}


	@Bean(name = "nfse")
	public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema xsdSchema) {
		
		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName   ("NfsePort");
		wsdl11Definition.setLocationUri    ("/ws");
		wsdl11Definition.setTargetNamespace("http://www.ginfes.com.br/");
		wsdl11Definition.setSchema         (xsdSchema);
		
		return wsdl11Definition;
	}

	@Bean
	public XsdSchema XsdSchema() {
		
		Resource  classPathResource = new ClassPathResource("xsd/vchNfse.xsd");
		XsdSchema simpleXsdSchema   = new SimpleXsdSchema(classPathResource);
		
		return simpleXsdSchema;
	}
}
