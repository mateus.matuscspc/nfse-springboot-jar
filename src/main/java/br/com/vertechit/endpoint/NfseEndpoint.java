package br.com.vertechit.endpoint;

import org.springframework.ws.server.endpoint.annotation.Endpoint;

@Endpoint(value="NfseEndPoint")
public class NfseEndpoint {
	
	/*
	private static final String NAMESPACE_URI = "http://www.ginfes.com.br/";

	@Autowired
	private ServiceRecepcionarLoteRpsV3 serviceRecepcionarLoteRpsV3;
	
	@Autowired
	private ServiceConsultarLoteRpsV3 serviceConsultarLoteRpsV3;
	
	@Autowired
	private ServiceConsultarNfseV3 serviceConsultarNfseV3;
	
	@Autowired
	private ServiceConsultarNfsePorRpsV3 serviceConsultarNfsePorRpsV3;
	
	@Autowired
	private ServiceCancelarNfseV3 serviceCancelarNfseV3;
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "enviarLoteRpsEnvioRequest")
	@ResponsePayload
	public EnviarLoteRpsRespostaResponse enviarLoteRpsEnvioRequest(@RequestPayload EnviarLoteRpsEnvioRequest request) {
		
		EnviarLoteRpsResposta enviarLoteRpsResposta = this.serviceRecepcionarLoteRpsV3.servicoProxy(request.getEnviarLoteRpsEnvio(), request.getTipo());
		
		EnviarLoteRpsRespostaResponse response = new EnviarLoteRpsRespostaResponse();
		response.setEnviarLoteRpsResposta(enviarLoteRpsResposta);
		
		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "consultarLoteRpsEnvioRequest")
	@ResponsePayload
	public ConsultarLoteRpsRespostaResponse consultarLoteRpsEnvioRequest(@RequestPayload ConsultarLoteRpsEnvioRequest request) {
		
		ConsultarLoteRpsResposta consultarLoteRpsResposta = this.serviceConsultarLoteRpsV3.servicoProxy(request.getConsultarLoteRpsEnvio(), request.getTipo());
		
		ConsultarLoteRpsRespostaResponse response = new ConsultarLoteRpsRespostaResponse();
		response.setConsultarLoteRpsResposta(consultarLoteRpsResposta);
		
		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "consultarNfseEnvioRequest")
	@ResponsePayload
	public ConsultarNfseRespostaResponse consultarNfseEnvioRequest(@RequestPayload ConsultarNfseEnvioRequest request) {
		
		ConsultarNfseResposta consultarNfseResposta = this.serviceConsultarNfseV3.servicoProxy(request.getConsultarNfseEnvio(), request.getTipo());
		
		ConsultarNfseRespostaResponse response = new ConsultarNfseRespostaResponse();
		response.setConsultarNfseResposta(consultarNfseResposta);
		
		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "consultarNfseRpsEnvioRequest")
	@ResponsePayload
	public ConsultarNfseRpsRespostaResponse consultarNfseRpsEnvioRequest(@RequestPayload ConsultarNfseRpsEnvioRequest request) {
		
		ConsultarNfseRpsResposta consultarNfseRpsResposta = this.serviceConsultarNfsePorRpsV3.servicoProxy(request.getConsultarNfseRpsEnvio(), request.getTipo());
		
		ConsultarNfseRpsRespostaResponse response = new ConsultarNfseRpsRespostaResponse();
		response.setConsultarNfseRpsResposta(consultarNfseRpsResposta);
		
		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "cancelarNfseEnvioRequest")
	@ResponsePayload
	public CancelarNfseRespostaResponse cancelarNfseEnvioRequest(@RequestPayload CancelarNfseEnvioRequest request) {
		
		CancelarNfseResposta cancelarNfseResposta = this.serviceCancelarNfseV3.servicoProxy(request.getCancelarNfseEnvio(), request.getTipo());
		
		CancelarNfseRespostaResponse response = new CancelarNfseRespostaResponse();
		response.setCancelarNfseResposta(cancelarNfseResposta);
		
		return response;
	}
	*/
}